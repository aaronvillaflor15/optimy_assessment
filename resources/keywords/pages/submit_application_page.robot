
*** Variables ***
@{my_tool_list}    Spreadsheet    Test management tool (any)    SQL, MySQL, MySQLi    Java    Javascript    Python    TestNG    Cucumber    Robot Framework

***Keywords***
When User submits a new application
    [Documentation]   Create new application for Optimy
    Navigate To New Submit New Application
	Fill Out Personal Details
	Fill Out Role Details

And Verify inputted data in summary screen
    [Documentation]   Verify inputted data is correct
    Click Next Screen
	Verify Data in Summary Screen

Then User successfully submitted the application
    [Documentation]   Submit the new application
    Click Validate And Send

And User should see thank you for submitting page
    [Documentation]   Verify that user is in thank you for submitting page
    Verify Page Is Thank You For Submitting

Navigate To New Submit New Application
    Click Element    ${home_page["button"]["submit_new_application"]}
    Scroll Element Into View    ${submit_application_page["button"]["submit_new_application"]}
    Click Element    ${submit_application_page["button"]["submit_new_application"]}

Fill Out Personal Details
    Input Text     ${submit_application_page["personal_details"]["text_field"]["first_name"]}    ${new_application["valid"]["first_name"]}
    Input Text    ${submit_application_page["personal_details"]["text_field"]["last_name"]}    ${new_application["valid"]["last_name"]}
    Input Text    ${submit_application_page["personal_details"]["text_field"]["address"]}    ${new_application["valid"]["address"]}
    Click Element    ${submit_application_page["personal_details"]["text_field"]["postal_code"]}  
    Input Text    ${submit_application_page["personal_details"]["text_field"]["postal_code"]}    ${new_application["valid"]["postal_code"]}
    Wait Until Element Is Visible    css:li[class='ui-menu-item']:nth-child(1) a[class='ui-menu-item-wrapper']
    Click Element    css:li[class='ui-menu-item']:nth-child(1) a[class='ui-menu-item-wrapper']
    Click Element    ${submit_application_page["personal_details"]["text_field"]["country"]}
    Select From List By Value    ${submit_application_page["personal_details"]["text_field"]["country"]}    ${new_application["valid"]["country"]}
    Choose File    ${submit_application_page["personal_details"]["text_field"]["photo"]}    ${EXECDIR}${/}assets${/}images${/}test_img.png
    Scroll Element Into View    ${submit_application_page["personal_details"]["button"]["male_radio_btn"]}
    Click Element    ${submit_application_page["personal_details"]["button"]["male_radio_btn"]}

Fill Out Role Details
    Scroll Element Into View    ${submit_application_page["role"]["text_field"]["role_applying"]}
    Click Element    ${submit_application_page["role"]["text_field"]["role_applying"]}
    Select From List By Index    ${submit_application_page["role"]["text_field"]["role_applying"]}    2
    Click Element    ${submit_application_page["role"]["checkbox"]["spreadsheet"]}
    Click Element    ${submit_application_page["role"]["checkbox"]["test_management"]}
    Click Element    ${submit_application_page["role"]["checkbox"]["sql_mysql"]}
    Click Element    ${submit_application_page["role"]["checkbox"]["java"]}
    Click Element    ${submit_application_page["role"]["checkbox"]["javascript"]}
    Click Element    ${submit_application_page["role"]["checkbox"]["python"]}   
    Click Element    ${submit_application_page["role"]["checkbox"]["testng"]}  
    Click Element    ${submit_application_page["role"]["checkbox"]["cucumber"]}  
    Click Element    ${submit_application_page["role"]["checkbox"]["robot_framework"]}

  
    Scroll Element Into View    ${submit_application_page["role"]["text_field"]["career_objective_frame"]}
    Select Frame    ${submit_application_page["role"]["text_field"]["career_objective_frame"]}
    Input Text    ${submit_application_page["role"]["text_field"]["career_objective"]}    ${new_application["valid"]["career_objective"]}
    Unselect Frame

Click Next Screen
    Wait Until Element Is Visible    ${submit_application_page["button"]["next_screen"]}
    Click Element    ${submit_application_page["button"]["next_screen"]}
    Wait Until Element Is Visible    ${submit_application_page["summary"]["text_label"]["summary_section"]}
    Wait Until Element Is Visible    ${submit_application_page["summary"]["text_label"]["first_name"]}

Verify Data in Summary Screen
    ${first_name} =    Get Text    ${submit_application_page["summary"]["text_label"]["first_name"]}
    ${last_name} =    Get Text    ${submit_application_page["summary"]["text_label"]["last_name"]}
    ${address} =    Get Text    ${submit_application_page["summary"]["text_label"]["house_number"]}
    ${postal_code} =    Get Text    ${submit_application_page["summary"]["text_label"]["postal_code"]}
    ${country} =    Get Text    ${submit_application_page["summary"]["text_label"]["country"]}
    ${photo} =    Get Text    ${submit_application_page["summary"]["text_label"]["photo"]}
    ${role} =    Get Text    ${submit_application_page["summary"]["text_label"]["role"]}        
    @{tools_name}    Get WebElements    css:div[class='question question-checkbox view'] li
	
    ${tools_list}    Create List
    FOR    ${tool_name}   IN      @{tools_name}
        ${text}    Get Text    ${tool_name}
        Append To List      ${tools_list}     ${text}
    END

    Should Be Equal    ${first_name}    ${new_application["valid"]["first_name"]}
    Should Be Equal    ${last_name}     ${new_application["valid"]["last_name"]}
    Should Be Equal    ${address}    ${new_application["valid"]["address"]}
    Should Be Equal    ${postal_code}     ${new_application["valid"]["postal_code"]}
    Should Be Equal    ${country}     Philippines
    Should Be Equal    ${photo}    test-img.png
    Lists Should Be Equal    ${tools_list}    ${my_tool_list}    
	${career_objective} =     Get Text    ${submit_application_page["summary"]["text_label"]["career_objective"]}

Click Validate And Send
    Scroll Element Into View   ${submit_application_page["summary"]["button"]["validate_send"]}
    Click Element    ${submit_application_page["summary"]["button"]["validate_send"]}
   
Verify Page Is Thank You For Submitting
    Wait Until Element Is Visible    ${submit_application_page["summary"]["text_label"]["thank_you_for_submitting"]}
    Page Should Contain Element    ${submit_application_page["summary"]["text_label"]["thank_you_for_submitting"]}   