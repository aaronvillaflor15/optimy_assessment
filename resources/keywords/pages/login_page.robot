***Keywords***
Perform Login
    [Documentation]   Login as a Optimy user
    [Arguments]    ${email}    ${password}
    Click Element    ${home_page["button"]["login"]} 
    Input Text    ${login_page["text_field"]["email"]}    ${email} 
    Input Text    ${login_page["text_field"]["password"]}    ${password}
    Click Element    ${login_page["button"]["login"]} 


Given A user has a valid login credentials
    Perform Login    ${login["valid"]["email"]}     ${login["valid"]["password"]}