submit_application_page = {
    "button": {
        "submit_new_application": "css:div[class='text-center d-flex flex-column align-items-center'] a[class='btn btn-outline-primary']",
        "next_screen": "id:navButtonNext"
    },
    "personal_details": {
        "text_field": {
            "first_name": "css:div[class='section edit floating-questions'] div[class='field '] input[aria-label='First name']",
            "last_name": "css:div[class='section edit floating-questions'] div[class='field '] input[aria-label='Last name']",
            "address": "css:div[class='section edit'] div[class='field'] textarea[aria-label='Unit no/House no, Street']",
            "postal_code": "css:div[class='section edit'] div[class='answer '] input[type='text']",
            "country": "css:div[class='question question-locationCountry edit question-required 1'] select[aria-label='Country']",
            "photo": "css:input[type='file'][name='Filedata']",
        },
        "button": {
            "male_radio_btn": "css:div[class='question question-radio edit question-required 1'] li[class='answer edit']:nth-child(1)",
        }
    },
    "role": {
        "text_field": {
            "role_applying": "css:div[class='question question-dropdown edit question-required 1'] select[class='custom-select']",
            "career_objective_frame": "css:iframe[class='cke_wysiwyg_frame cke_reset']",
            "career_objective": "css:body[class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']",
        },
        "checkbox": {
            "spreadsheet": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(1)",
            "test_management": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(3)",
            "sql_mysql": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(4)",
            "java": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(5)",
            "javascript": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(6)",
            "python": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(7)",
            "testng": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(8)",
            "cucumber": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(9)",
            "robot_framework": "css:div[class='question question-checkbox edit question-required 1'] li[class='answer edit radio-checkbox-li-element']:nth-child(10)",
        }
    },
    "summary": {
        "text_label": {
            "summary_section": "css:a[class='card-header h2 stepper__label stepper__label--active'] #screenNo",
            "first_name": "css:div[class='question question-text view']:nth-child(2) div[class='field']",
            "last_name": "css:div[class='question question-text view']:nth-child(3) div[class='field']",
            "house_number": "css:div[class='mb-3 answer view'] p[class='mb-0']",
            "postal_code": "css:div[class='mb-3    answer'] p[class='mb-0    field']",
            "country": "css:div[class='mb-3    answer'] p[class='mb-0   field']",
            "photo": "css:div[class='form-group answers'] div[class='mb-3    answer view'] a",
            "role": "css:div[class='question question-dropdown view'] p[class='answer view mb-0 ']",
            "career_objective": "css:div[class='question question-richtext view'] p[class='mb-0 field']",
            "thank_you_for_submitting": "css:h1[class='h1 text-center']"
        },
        "button": {
            "validate_send": "css:div[class='mt-4 mb-2 mt-md-0 card-footer d-none d-md-flex justify-content-between align-items-center'] button[class='btn btn-primary ml-md-auto'][type='submit']"

        }
    },
}