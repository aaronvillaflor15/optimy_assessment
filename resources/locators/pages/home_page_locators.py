home_page = {
    "button": {
        'login': "css:.ml-auto.btn.btn-outline-primary",
        "cookie_close": "id:cookie-close",
        "submit_new_application": "css:a[class='btn btn-primary btn-lg col-md-auto'][href='project/new/']"
    },
}