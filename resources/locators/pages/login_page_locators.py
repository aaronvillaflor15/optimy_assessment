login_page = {
    "text_field": {
        "email": "id:login-email",
        "password": "id:login-password",
    },
    "button": {
        "login": "css:button[class='btn btn-lg btn-primary col-12 mt-1 mt-md-2'][type='submit']"
    }
}