*** Settings ***
Library    SeleniumLibrary
Library    BuiltIn
Library    Collections
Resource    ${CURDIR}${/}..${/}..${/}resources${/}keywords${/}pages${/}home_page.robot
Resource    ${CURDIR}${/}..${/}..${/}resources${/}keywords${/}pages${/}login_page.robot
Resource    ${CURDIR}${/}..${/}..${/}resources${/}keywords${/}pages${/}submit_application_page.robot
Variables    ${CURDIR}${/}..${/}..${/}resources${/}locators${/}pages${/}home_page_locators.py
Variables    ${CURDIR}${/}..${/}..${/}resources${/}locators${/}pages${/}login_page_locators.py
Variables    ${CURDIR}${/}..${/}..${/}resources${/}locators${/}pages${/}submit_application_page_locators.py
Variables    ${CURDIR}${/}..${/}..${/}data${/}login${/}login.py
Variables    ${CURDIR}${/}..${/}..${/}data${/}application${/}new_application.py
Test Setup    Go To Optimy Home Page    


***Test Cases***
Create new application
    [Tags]    new_application
    [Documentation]   Create new application for Optimy
    Given A user has a valid login credentials
    When User submits a new application
    And Verify inputted data in summary screen
    Then User successfully submitted the application
    And User should see thank you for submitting page
   