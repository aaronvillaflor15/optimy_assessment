# optimy_assessment

## Robot Framework - Installation (WINDOWS OS)
    - Download and Install Python 3
    - Check if PIP 3 is installed 
               pip --version
    - Create Virtual Environment
        1.  Go to your preferred project directory by running the following command in your
            Command Prompt.
               cd path\to\preferred\directory
        2.  In there, run the following command    
            python -m venv env
        3.  Activate the virtual environment by running the following command in your Command
            Prompt.    
               env\Scripts\activate
        4. Check if the line starts with (env)
    - Update Google chrom and install latest ChromeDriver
    - Install Python package 
        1.  Create a text file and name it as requirements.txt
        2.  Paste the following text inside the text file and save it.
                robotframework==4.0rc1
                robotframework-seleniumlibrary
        3.  Put requirements.txt inside your preferred project directory
        4.  Run the following command in your Command
              pip install -r requirements.txt



## How to run test
   - to run test activate your virtual env 
         env\Scripts\activate
   - then run the following command
          robot tests      



        
